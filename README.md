# DataMining

A data mining software with some algorithms.

## Development Environment
* Eclipse 4.2.2 (Juno)
* JDK 1.5

## Code Structure
* algorithm -- algorithm set, add what you want
  * ID3 -- ID3 implementation
  * Kmeans -- K-means implementation
* data -- data structure
  * DataSet.java -- data set class
  * Attribute.java -- attribute class
  * Instance.java -- Instance (data) class
* gui -- gui set
  * Main.java -- main panel
  * AnalysisPanel.java -- analysis panel
  * DataPanel.java -- data panel
* util -- tool set
  * Reader.java -- read data tool

## Using

### main panel
![](./jpg/main.jpg "main panel")

### load file
![](./jpg/file.jpg "load file")

### data panel
![](./jpg/data.jpg "data panel")

### ID3
![](./jpg/ID3.jpg "ID3")

### K-means
![](./jpg/Kmeans.jpg "K-means")

## License
[Apache License 2.0](LICENSE)